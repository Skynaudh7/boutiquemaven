/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.skynaudh.entites.Achat;
import com.skynaudh.entites.Categorie;
import java.util.*;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author Yassin
 */
@Path("/AchatService")
public class AchatResource {
    static List<Achat> liste;
    
    @POST
    public void ajouter(@PathParam("Achat1") Achat e){
    liste.add(e);
    }
    public void modifier(@PathParam("idAchat") int id, @PathParam("Achat") Achat e){
    liste.set(id, e);
    }
    public Achat trouver(@PathParam("idAchat") int id){
    return liste.get(id);
    }
    public void supprimer(@PathParam("idAchat") int id){
    liste.remove(id);
    }
    public void supprimer(@PathParam("Achat") Achat e){
    liste.remove(e);
    };
    
    @GET
     public List<Achat> lister(){
       for(int i=0; i< liste.size(); i++){
           System.out.println(" "+liste.get(i)); 
       }
        return null;
   }
     public List<Achat> lister(@PathParam("ValDebut") int debut, @PathParam("ValNombre") int nombre){
      for (int i=debut; i< nombre; i++){
           System.out.println(" "+liste.get(i)); 
      }
      return null;
  };
    
}
