/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.skynaudh.entites.Employe;
import java.util.*;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author Yassin
 */
@Path("/EmployeService")
public class EmployeResource {
    static List<Employe> liste;
    
    @POST
    public void ajouter(@PathParam("Employe") Employe e){
    liste.add(e);
    }
    public void modifier(@PathParam("idEmploye") int id, @PathParam("Employe") Employe e){
    liste.set(id, e);
    }
    public Employe trouver(@PathParam("idEmploye") int id){
    return liste.get(id);
    }
    public void supprimer(@PathParam("idEmploye") int id){
    liste.remove(id);
    }
    public void supprimer(@PathParam("Employe") Employe e){
    liste.remove(e);
    };
    
    @GET
     public List<Employe> lister(){
       for(int i=0; i< liste.size(); i++){
           System.out.println(" "+liste.get(i)); 
       }
        return null;
   }
     public List<Employe> lister(@PathParam("ValDebut") int debut, @PathParam("ValNombre") int nombre){
      for (int i=debut; i< nombre; i++){
           System.out.println(" "+liste.get(i)); 
      }
      return null;
  };
}
