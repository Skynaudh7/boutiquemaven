/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.skynaudh.entites.Produit;
import java.util.*;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author Yassin
 */
@Path("/ProduitService")
public class ProduitResource {
    static List<Produit> liste;
    
    @POST
    public void ajouter(@PathParam("Produit1") Produit e){
    liste.add(e);
    }
    public void modifier(@PathParam("idProduit") int id, @PathParam("Produit1") Produit e){
    liste.set(id, e);
    }
    public Produit trouver(@PathParam("idProduit") int id){
    return liste.get(id);
    }
    public void supprimer(@PathParam("idProduit") int id){
    liste.remove(id);
    }
    public void supprimer(@PathParam("Produit1") Produit e){
    liste.remove(e);
    };
    
    @GET
     public List<Produit> lister(){
       for(int i=0; i< liste.size(); i++){
           System.out.println(" "+liste.get(i)); 
       }
        return null;
   }
     public List<Produit> lister(@PathParam("ValDebut") int debut, @PathParam("ValNombre") int nombre){
      for (int i=debut; i< nombre; i++){
           System.out.println(" "+liste.get(i)); 
      }
      return null;
  };
}
