/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.skynaudh.entites.Categorie;
import java.util.*;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author Yassin
 */
@Path("/CategorieService")
public class jaxRsResource {
    static List<Categorie> liste;
    
    @POST
    public void ajouter(@PathParam("categorie1") Categorie e){
    liste.add(e);
    }
    public void modifier(@PathParam("idCategorie") int id, @PathParam("categorie1") Categorie e){
    liste.set(id, e);
    }
    public Categorie trouver(@PathParam("idCategorie") int id){
    return liste.get(id);
    }
    public void supprimer(@PathParam("idProduit") int id){
    liste.remove(id);
    }
    public void supprimer(@PathParam("categorie1") Categorie e){
    liste.remove(e);
    };
    
    @GET
     public List<Categorie> lister(){
       for(int i=0; i< liste.size(); i++){
           System.out.println(" "+liste.get(i)); 
       }
        return null;
   }
     public List<Categorie> lister(@PathParam("ValDebut") int debut, @PathParam("ValNombre") int nombre){
      for (int i=debut; i< nombre; i++){
           System.out.println(" "+liste.get(i)); 
      }
      return null;
  };
}
