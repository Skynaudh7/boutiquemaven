/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.skynaudh.entites.Client;
import java.util.*;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author Yassin
 */
@Path("/ClientService")
public class ClientResource {
    static List<Client> liste;
    
    @POST
    public void ajouter(@PathParam("Client") Client e){
    liste.add(e);
    }
    public void modifier(@PathParam("idClient") int id, @PathParam("Client") Client e){
    liste.set(id, e);
    }
    public Client trouver(@PathParam("idClient") int id){
    return liste.get(id);
    }
    public void supprimer(@PathParam("idClient") int id){
    liste.remove(id);
    }
    public void supprimer(@PathParam("idClient") Client e){
    liste.remove(e);
    };
    
    @GET
     public List<Client> lister(){
       for(int i=0; i< liste.size(); i++){
           System.out.println(" "+liste.get(i)); 
       }
        return null;
   }
     public List<Client> lister(@PathParam("ValDebut") int debut, @PathParam("ValNombre") int nombre){
      for (int i=debut; i< nombre; i++){
           System.out.println(" "+liste.get(i)); 
      }
      return null;
  };
}
