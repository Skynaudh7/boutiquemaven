/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs;

import com.skynaudh.entites.ProduitAchete;
import java.util.*;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author Yassin
 */
@Path("/ProduitAcheteService")
public class ProduitAcheteResource {
    static List<ProduitAchete> liste;
    
    @POST
    public void ajouter(@PathParam("ProduitAchete") ProduitAchete e){
    liste.add(e);
    }
    public void modifier(@PathParam("idProduitAchete") int id, @PathParam("ProduitAchete") ProduitAchete e){
    liste.set(id, e);
    }
    public ProduitAchete trouver(@PathParam("idProduitAchete") int id){
    return liste.get(id);
    }
    public void supprimer(@PathParam("idProduitAchete") int id){
    liste.remove(id);
    }
    public void supprimer(@PathParam("ProduitAchete") ProduitAchete e){
    liste.remove(e);
    };
    
    @GET
     public List<ProduitAchete> lister(){
       for(int i=0; i< liste.size(); i++){
           System.out.println(" "+liste.get(i)); 
       }
        return null;
   }
     public List<ProduitAchete> lister(@PathParam("ValDebut") int debut, @PathParam("ValNombre") int nombre){
      for (int i=debut; i< nombre; i++){
           System.out.println(" "+liste.get(i)); 
      }
      return null;
  };
}
