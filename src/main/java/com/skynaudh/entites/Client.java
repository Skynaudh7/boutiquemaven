/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skynaudh.entites;

import java.beans.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Yassin
 */
public class Client extends Personne implements Serializable {
    
    private String cin;
    private String carteVisa;
    List<Achat> achat;
    
     public Client(long id, String nom, String prenom, LocalDate date_naissance) {
        super(id, nom, prenom, date_naissance);
    }
    public Client(long id, String nom, String prenom, LocalDate date_naissance, String cin, String carteVisa) {
        super(id, nom, prenom, date_naissance);
        this.cin = cin;
        this.carteVisa = carteVisa;
    }
    
    
///////////////     GETTERS     SETTERS     ////////////////////////////////////

    public String getCin() {
        return cin;
    }

    public String getCarteVisa() {return carteVisa;}

    public void setCin(String cin) { this.cin = cin;}

    public void setCarteVisa(String carteVisa) {this.carteVisa = carteVisa; }
    
    public void setAchat(List<Achat> achat) { this.achat = achat; }
    
    public List<Achat> getAchat() {return achat;}
    
  
    
 /////////////////////  HASHCODE        EQUALS      ////////////////////////

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.cin);
        hash = 59 * hash + Objects.hashCode(this.carteVisa);
        hash = 59 * hash + Objects.hashCode(this.achat);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Client other = (Client) obj;
        if (!Objects.equals(this.cin, other.cin)) {
            return false;
        }
        if (!Objects.equals(this.carteVisa, other.carteVisa)) {
            return false;
        }
        if (!Objects.equals(this.achat, other.achat)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Client d'identifiant : " +this.getId()+ ", de nom : " + this.getNom() + " " + this.getPrenom() + " "+this.getAge()+ " ans" ;
    }
    
    
}
