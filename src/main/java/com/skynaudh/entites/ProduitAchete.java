/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skynaudh.entites;

import java.beans.*;
import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Yassin
 */
public class ProduitAchete implements Serializable {
    
    private int quantite = 1;
    private double remise = 0;
    private Produit produit;
    private Achat achat;

    public ProduitAchete(Produit produit, Achat achat) {
        this.produit = produit;
        this.achat = achat;
    }
    
    
///////////////////////////    METHODES    ///////////////////////////////
    

    public double getPrixTotal(){
       return (produit.getPrixUnitaire() * this.quantite)- this.remise;
    }
    
    
    
    public int getQuantite() { return quantite; }

    public double getRemise() { return remise;}

    public Produit getProduit() {return produit;}

    public void setQuantite(int quantite) { this.quantite = quantite; }

    public void setRemise(double remise) {this.remise = remise;}

    public void setProduit(Produit produit) {this.produit = produit;}
    
    public Achat getAchat() { return achat;}

    public void setAchat(Achat achat) { this.achat = achat;}
    
    
    
 
////////////////////////        HASHCODE        EQUALS      //////////////////////////
    
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + this.quantite;
        hash = 89 * hash + (int) (Double.doubleToLongBits(this.remise) ^ (Double.doubleToLongBits(this.remise) >>> 32));
        hash = 89 * hash + Objects.hashCode(this.produit);
        hash = 89 * hash + Objects.hashCode(this.achat);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProduitAchete other = (ProduitAchete) obj;
        if (this.quantite != other.quantite) {
            return false;
        }
        if (Double.doubleToLongBits(this.remise) != Double.doubleToLongBits(other.remise)) {
            return false;
        }
        if (!Objects.equals(this.produit, other.produit)) {
            return false;
        }
        if (!Objects.equals(this.achat, other.achat)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ProduitAchete{" + "quantite=" + quantite + ", remise=" + remise + ", produit=" + produit + ", achat=" + achat + '}';
    }
    
    
    
}
