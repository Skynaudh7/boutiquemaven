/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skynaudh.entites;

import java.beans.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author Yassin
 */
public class Personne implements Serializable {
    
     private long id;
    private String nom;
    private String prenom;
    private LocalDate date_naissance;
   

    public Personne(long id, String nom, String prenom, LocalDate date_naissance) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.date_naissance = date_naissance;
    }
    
    
 ///////////////////        METHODES        ///////////////////////////
    
    
    
    public int getAge(){
        return LocalDate.now().getYear() - this.date_naissance.getYear();
    }
    
     public int getAge(LocalDate ref){
        return LocalDate.now().getYear()- ref.getYear();
    }
     
     
     
    
    
//////////////////////////////   GETTERS        SETTERS     ////////////////////////
    
   

    public void setId(long id) {this.id = id;}

    public void setNom(String nom) { this.nom = nom;}

    public void setPrenom(String prenom) { this.prenom = prenom;}

    public void setDate_naissance(LocalDate date_naissance) {this.date_naissance = date_naissance;}

    public long getId() { return id;}

    public String getNom() { return nom; }

    public LocalDate getDate_naissance() {return date_naissance;}

    public String getPrenom() {return prenom;}
    
    
    
    
 //////////////////     HASHCODE        EQUALS      //////////////////////////////
    
    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 41 * hash + Objects.hashCode(this.nom);
        hash = 41 * hash + Objects.hashCode(this.prenom);
        hash = 41 * hash + Objects.hashCode(this.date_naissance);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {return true;}
        if (obj == null) {return false;}
        if (getClass() != obj.getClass()) { return false;}
        final Personne other = (Personne) obj;
        if (this.id != other.id) { return false;}
        if (!Objects.equals(this.nom, other.nom)) {return false;}
        if (!Objects.equals(this.prenom, other.prenom)) { return false;}
        if (!Objects.equals(this.date_naissance, other.date_naissance)) {return false;}
        return true;
    }

   
    
   
    @Override
    public String toString() {
        return "Personne ayant l'Id : "  + this.id + " , de nom=" + this.nom + ", de prenom=" + prenom + ", et de date de naissance=" + date_naissance ;
    }
}
