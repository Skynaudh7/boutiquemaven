/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skynaudh.entites;

import java.beans.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Yassin
 */
public class Achat implements Serializable {
    
    private long id;
    private double remise =0;
    LocalDate dateAchat;
    List<ProduitAchete> produitAchete;
    private Employe employe;
    private Client client;

    public Achat(long id, double remise, LocalDate dateAchat, Employe employe, Client client) {
        this.id = id;
        this.remise = remise;
        this.dateAchat = dateAchat;
        this.employe = employe;
        this.client = client;
    }
    
    
//////////////////          METHODES        ///////////////////////////
    
     public double getPrixTotal(){
        double prix = 0;
        for(int i=0; i < produitAchete.size(); i++){
            prix += produitAchete.get(i).getPrixTotal();
        }
        return prix;
    }
    
    public double getRemiseTotal(){
        for(int i=0; i<produitAchete.size(); i++){
            this.remise += produitAchete.get(i).getRemise();
        }
        return this.remise;
    }
    
    
////////////////////    GETTERS         SETTERS     ///////////////////////////
    
    

    public long getId() { return id;}

    public double getRemise() { return remise; }

    public LocalDate getDateAchat() {  return dateAchat; }

    public List<ProduitAchete> getProduitAchete() {return produitAchete; }

    public Client getClient() { return client; }

    public void setClient(Client client) { this.client = client;}

    public Employe getEmploye() {return employe;}

    public void setId(long id) { this.id = id;}

    public void setRemise(double remise) {this.remise = remise; }

    public void setDateAchat(LocalDate dateAchat) {this.dateAchat = dateAchat;}
    
    public void setProduitAchete(List<ProduitAchete> produitAchete) { this.produitAchete = produitAchete;}

    public void setEmploye(Employe employe) { this.employe = employe;}
    
 
    
///////////////////     HASHCODE     EQUALS     /////////////////////////////////////

    
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 47 * hash + (int) (Double.doubleToLongBits(this.remise) ^ (Double.doubleToLongBits(this.remise) >>> 32));
        hash = 47 * hash + Objects.hashCode(this.dateAchat);
        hash = 47 * hash + Objects.hashCode(this.produitAchete);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Achat other = (Achat) obj;
        if (this.id != other.id) {
            return false;
        }
        if (Double.doubleToLongBits(this.remise) != Double.doubleToLongBits(other.remise)) {
            return false;
        }
        if (!Objects.equals(this.dateAchat, other.dateAchat)) {
            return false;
        }
        if (!Objects.equals(this.produitAchete, other.produitAchete)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Achat{" + "id=" + id + ", remise=" + remise + ", dateAchat=" + dateAchat + ", produitAchete=" + produitAchete + ", employe=" + employe + ", client=" + client + '}';
    }

    
    

    

    
    
    
}
