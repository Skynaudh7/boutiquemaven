/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skynaudh.entites;

import java.beans.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Yassin
 */
public class Categorie implements Serializable {
    
     private long id;
    private String libelle;
    private String description;
    List<Produit> produits;
    
    
    public Categorie(long id, String libelle, String description){
        this.id = id;
        this.libelle = libelle;
        this.description = description;
    }
    
    
 
    
////////////////////////////  GETTERS     SETTERS      //////////////////////////////////
    
    
    

    public void setProduits(List<Produit> produits) {
        this.produits = produits;
    }

    public List<Produit> getProduits() {
        return produits;
    }

    public long getId() {
        return id;
    }

    public String getLibelle() {
        return libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
    
/////////////////////////////   METHODES  EQUALS    HASHCODE     TOSTRING   /////////////////////////
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 23 * hash + Objects.hashCode(this.libelle);
        hash = 23 * hash + Objects.hashCode(this.description);
        hash = 23 * hash + Objects.hashCode(this.produits);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Categorie other = (Categorie) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.libelle, other.libelle)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.produits, other.produits)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Categorie{" + "id=" + id + ", libelle=" + libelle + ", description=" + description + ", produits=" + produits + '}';
    }
    
    
    
    
}
