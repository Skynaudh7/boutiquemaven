/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skynaudh.entites;

import java.beans.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Yassin
 */
public class Employe extends Personne implements Serializable {
    
    private String cnss;
    private LocalDate date_embauche;
    List<Achat> achat;
    
    
    public Employe(long id, String nom, String prenom, LocalDate date_naissance) {
        super(id, nom, prenom, date_naissance);
    }
    
    public Employe(long id, String nom, String prenom, LocalDate date_naissance, String cnss, LocalDate date_embauche) {
        super(id, nom, prenom, date_naissance);
        this.cnss = cnss;
        this.date_embauche = date_embauche;
    }

    
    
/////////////////////////////       GETTERS     SETTERS     ///////////////////////////
    
    
    
    public String getCnss() {return cnss;}

    public LocalDate getDate_embauche() {return date_embauche;}

    public List<Achat> getAchat() {return achat;}

    public void setCnss(String cnss) { this.cnss = cnss;}

    public void setDate_embauche(LocalDate date_embauche) {this.date_embauche = date_embauche;}

    public void setAchat(List<Achat> achat) {this.achat = achat;}

    
    
    
/////////////////////////////////       METHODES    EQUALS      HASHCODE        ///////////////////////////
    
    
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.cnss);
        return hash;
    }

    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {return true;}
        if (obj == null) {return false;}
        if (getClass() != obj.getClass()) {return false;}
        
        final Employe other = (Employe) obj;
        if (!Objects.equals(this.cnss, other.cnss)) {return false;}
        if (!Objects.equals(this.date_embauche, other.date_embauche)) {return false;}
        return true;
    }

    @Override
    public String toString() {
        return "Employe d'identiant : " +this.getId() +" de nom " +this.getNom()+ "  " +this.getPrenom()+ " , date_embauche=" + this.date_embauche;
    }

    
    
    
    
}
