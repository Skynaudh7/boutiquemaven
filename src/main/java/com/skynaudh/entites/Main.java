/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skynaudh.entites;

import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author Yassin
 */
public class Main {
 
    
     public static void main(String[] args) {
        // TODO code application logic here
        Client client = new Client(1,"BRAND", "Jean", LocalDate.of(1986, 12, 24));
        Employe employe = new Employe(14,"Rolo", "Temari", LocalDate.of(2000, 11, 23));
        Categorie categorie421 = new Categorie(421,"Electronique", "Arduino, C++");
        Produit produit1 = new Produit(14, "Connecteur Caméra", 2500, LocalDate.now(), categorie421);
        Produit produit2 = new Produit(17, "Micro-processeur", 500, LocalDate.now(), categorie421);
        
        Achat achat = new Achat(21, 0, LocalDate.now(),employe,client);
        
        ProduitAchete produitAchete1 = new ProduitAchete(produit1, achat);
        ProduitAchete produitAchete2 = new ProduitAchete(produit2, achat);
        
        
        achat.setProduitAchete((List<ProduitAchete>) produitAchete1);
        achat.setProduitAchete((List<ProduitAchete>) produitAchete2);
        
        
        System.out.println("--------------------------------------");
        System.out.println(achat.toString());
        System.out.println("--------------------------------------");
        System.out.println(produit1.toString());
        System.out.println("--------------------------------------");
        System.out.println(produit2.toString());
        System.out.println("--------------------------------------");
        System.out.println(produitAchete1.toString());
        System.out.println("--------------------------------------");
        System.out.println(produitAchete2.toString());
        System.out.println("--------------------------------------");
        System.out.println(client.toString());
        System.out.println("--------------------------------------");
        System.out.println(employe.toString());
    }
    
    
}
