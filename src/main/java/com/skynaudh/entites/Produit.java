/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.skynaudh.entites;

import java.beans.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Yassin
 */
public class Produit implements Serializable {
    
    private long id;
    private String libelle;
    private double prixUnitaire;
    LocalDate datePeremption;
    private Categorie categorie;
    List<ProduitAchete> produitAchete;

    public Produit(long id, String libelle, double prixUnitaire, LocalDate datePeremption, Categorie categorie) {
        this.id = id;
        this.libelle = libelle;
        this.prixUnitaire = prixUnitaire;
        this.datePeremption = datePeremption;
        this.categorie = categorie;
    }
    
    
//////////////////////      METHODES        ///////////////////
    
    
     public boolean estPerime(){
        if(this.datePeremption.compareTo(LocalDate.now())<= 0)
            return true;
        return false;
    }
    
     public boolean estPerime(LocalDate ref){
       if(datePeremption.compareTo(ref)<=0)
           return true;
       return false;
    }
     
     
     
///////////////////         GETTERS     SETTERS         /////////////////////////
     
     

    public void setProduitAchete(List<ProduitAchete> produitAchete) {
        this.produitAchete = produitAchete;
    }

    public List<ProduitAchete> getProduitAchete() {
        return produitAchete;
    }
    
    public void setId(long id) {this.id = id;}

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setPrixUnitaire(double prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public void setDatePeremption(LocalDate datePeremption) {
        this.datePeremption = datePeremption;
    }

    public long getId() {
        return id;
    }

    public String getLibelle() {
        return libelle;
    }

    public double getPrixUnitaire() {
        return prixUnitaire;
    }

    public LocalDate getDatePeremption() {
        return datePeremption;
    }
    

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    
   

     
     
//////////////////////////// METHODE   HASHCODE  EQUALS  ////////////////////////////////////////
     
     
     
     
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 23 * hash + Objects.hashCode(this.libelle);
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.prixUnitaire) ^ (Double.doubleToLongBits(this.prixUnitaire) >>> 32));
        hash = 23 * hash + Objects.hashCode(this.datePeremption);
        hash = 23 * hash + Objects.hashCode(this.categorie);
        hash = 23 * hash + Objects.hashCode(this.produitAchete);
        return hash;
    }

     

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Produit other = (Produit) obj;
        if (this.id != other.id) {
            return false;
        }
        if (Double.doubleToLongBits(this.prixUnitaire) != Double.doubleToLongBits(other.prixUnitaire)) {
            return false;
        }
        if (!Objects.equals(this.libelle, other.libelle)) {
            return false;
        }
        if (!Objects.equals(this.datePeremption, other.datePeremption)) {
            return false;
        }
        if (!Objects.equals(this.categorie, other.categorie)) {
            return false;
        }
        if (!Objects.equals(this.produitAchete, other.produitAchete)) {
            return false;
        }
        return true;
    }

   

     
    @Override
    public String toString() {
        return "Produit{" + "id=" + id + ", libelle=" + libelle + ", prixUnitaire=" + prixUnitaire + ", datePeremption=" + datePeremption + '}';
    }
    
    
    
}
