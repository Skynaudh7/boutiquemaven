/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.skynaudh.entites.*;
import java.util.*;

/**
 *
 * @author Yassin
 */
public class CategorieService {
    static List<Categorie> liste;
    
   public void ajouter(Categorie e){
       liste.add(e);
   }
   public void modifier(int id, Categorie e){
       liste.set(id, e);
   }
   public Categorie trouver(int id) {
     return liste.get(id);
   }
   public void supprimer(int id) {
       liste.remove(id);
   }
   public void supprimer(Categorie e) {
       liste.remove(e);
   }
  public List<Categorie> lister(){
       for(int i=0; i< liste.size(); i++){
           System.out.println(" "+liste.get(i)); 
       }
        return null;
   }
  public List<Categorie> lister(int debut, int nombre){
      for (int i=debut; i< nombre; i++){
           System.out.println(" "+liste.get(i)); 
      }
      return null;
  }
 
}
