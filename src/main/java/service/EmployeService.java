/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.skynaudh.entites.Achat;
import com.skynaudh.entites.Employe;
import java.util.List;

/**
 *
 * @author Yassin
 */
public class EmployeService {
    static List<Employe> liste;
    
   public void ajouter(Employe e){
       liste.add(e);
   }
   public void modifier(int id, Employe e){
       liste.set(id, e);
   }
   public Employe trouver(int id) {
     return liste.get(id);
   }
   public void supprimer(int id) {
       liste.remove(id);
   }
   public void supprimer(Employe e) {
       liste.remove(e);
   }
  public List<Employe> lister(){
       for(int i=0; i< liste.size(); i++){
           System.out.println(" "+liste.get(i)); 
       }
        return null;
   }
  public List<Employe> lister(int debut, int nombre){
      for (int i=debut; i< nombre; i++){
           System.out.println(" "+liste.get(i)); 
      }
      return null;
  }
    
}
