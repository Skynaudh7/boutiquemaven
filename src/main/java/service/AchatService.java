/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.skynaudh.entites.Achat;
import java.util.List;

/**
 *
 * @author Yassin
 */
public class AchatService {
    static List<Achat> liste;
    
   public void ajouter(Achat e){
       liste.add(e);
   }
   public void modifier(int id, Achat e){
       liste.set(id, e);
   }
   public Achat trouver(int id) {
     return liste.get(id);
   }
   public void supprimer(int id) {
       liste.remove(id);
   }
   public void supprimer(Achat e) {
       liste.remove(e);
   }
  public List<Achat> lister(){
       for(int i=0; i< liste.size(); i++){
           System.out.println(" "+liste.get(i)); 
       }
        return null;
   }
  public List<Achat> lister(int debut, int nombre){
      for (int i=debut; i< nombre; i++){
           System.out.println(" "+liste.get(i)); 
      }
      return null;
  }
    
}
