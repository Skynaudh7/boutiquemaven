/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.skynaudh.entites.Produit;
import java.util.List;

/**
 *
 * @author Yassin
 */
public class ProduitService {
    static List<Produit> liste;
    
   public void ajouter(Produit e){
       liste.add(e);
   }
   public void modifier(int id, Produit e){
       liste.set(id, e);
   }
   public Produit trouver(int id) {
     return liste.get(id);
   }
   public void supprimer(int id) {
       liste.remove(id);
   }
   public void supprimer(Produit e) {
       liste.remove(e);
   }
  public List<Produit> lister(){
       for(int i=0; i< liste.size(); i++){
           System.out.println(" "+liste.get(i)); 
       }
        return null;
   }
  public List<Produit> lister(int debut, int nombre){
      for (int i=debut; i< nombre; i++){
           System.out.println(" "+liste.get(i)); 
      }
      return null;
  }
    
}
