/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.skynaudh.entites.Client;
import java.util.List;

/**
 *
 * @author Yassin
 */
public class ClientService {
    static List<Client> liste;
    
   public void ajouter(Client e){
       liste.add(e);
   }
   public void modifier(int id, Client e){
       liste.set(id, e);
   }
   public Client trouver(int id) {
     return liste.get(id);
   }
   public void supprimer(int id) {
       liste.remove(id);
   }
   public void supprimer(Client e) {
       liste.remove(e);
   }
  public List<Client> lister(){
       for(int i=0; i< liste.size(); i++){
           System.out.println(" "+liste.get(i)); 
       }
        return null;
   }
  public List<Client> lister(int debut, int nombre){
      for (int i=debut; i< nombre; i++){
           System.out.println(" "+liste.get(i)); 
      }
      return null;
  }
    
}
