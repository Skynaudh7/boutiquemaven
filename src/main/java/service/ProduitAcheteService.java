/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.skynaudh.entites.ProduitAchete;
import java.util.List;

/**
 *
 * @author Yassin
 */
public class ProduitAcheteService {
    static List<ProduitAchete> liste;
    
   public void ajouter(ProduitAchete e){
       liste.add(e);
   }
   public void modifier(int id, ProduitAchete e){
       liste.set(id, e);
   }
   public ProduitAchete trouver(int id) {
     return liste.get(id);
   }
   public void supprimer(int id) {
       liste.remove(id);
   }
   public void supprimer(ProduitAchete e) {
       liste.remove(e);
   }
  public List<ProduitAchete> lister(){
       for(int i=0; i< liste.size(); i++){
           System.out.println(" "+liste.get(i)); 
       }
        return null;
   }
  public List<ProduitAchete> lister(int debut, int nombre){
      for (int i=debut; i< nombre; i++){
           System.out.println(" "+liste.get(i)); 
      }
      return null;
  }
    
}
